# client ID 
# Token
# discord permissions 67584

# https://discordapp.com/api/oauth2/authorize?client_id=500703776538099740&scope=bot&permissions=3221504


import random
import json
import requests
import asyncio
import emoji
import discord 
from discord.utils import get
from discord.ext import commands
from discord.ext.commands import Bot

random = random
token = ""
client = commands.Bot(command_prefix = '*', case_insensitive=True)

def civNum(civArr):
        civ = random.randint(0,42)
        while civ in civArr:
            civ = random.randint(0,42)
        return civ

with open('civ5.json') as f:
    data = json.load(f)

@client.event
async def on_ready():
    print("WE have logged in as DenBot")
    await client.change_presence(game=discord.Game(name="*help for commands",type=0))

async def update_data(users, user):
    if not user.id in users:
        users[user.id] = {}
        users[user.id]['experience'] = 0
        users[user.id]['level'] = 1
        users[user.id]['skillPoints'] = 1
        users[user.id]['CloutPoints'] = 5
        users[user.id]['CloutStats'] = {}
        users[user.id]['CloutStats']['Given'] = 0
        users[user.id]['CloutStats']['Gained'] = 0
        users[user.id]['CloutStats']['Lost'] = 0
        users[user.id]['Skills'] = {}
        users[user.id]['Skills']['Strength'] = 1
        users[user.id]['Skills']['Intelligence'] = 1
        users[user.id]['Skills']['Dexterity'] = 1
        users[user.id]['Skills']['Constitution'] = 1
        users[user.id]['Skills']['Wisdom'] = 1
        users[user.id]['Skills']['WillPower'] = 1
        users[user.id]['Skills']['Perception'] = 1
        users[user.id]['Skills']['Luck'] = 1

async def add_exp(users,user,exp):
    if user.id == 500703776538099740:
        return
    else:
        users[user.id]['experience'] += exp

async def level_up(users,user,channel):
    exp = users[user.id]['experience']
    lvl_start = users[user.id]['level']
    lvl_end = int(exp ** (1/4))


    if lvl_start < lvl_end:
        users[user.id]['level'] = lvl_end
        users[user.id]['skillPoints'] += 1
        await client.send_message(channel,'{} has leveled up to level {}, new skill point available!'.format(user.mention,lvl_end))
        

@client.command(pass_context=True)
async def lvlup(ctx):
    """ use to assign skill points, 
        *skillpoint str
        one point at a time
        str : Strength,
        int : Intelligence,
        dex : Dexterity,
        con : Constitution,
        wis : Wisdom,
        wil : WillPower,
        per : Perception,
        luc : Luck
    """
    with open('Levels.json','r') as f:
        users = json.load(f)
    
    messageEdit = ctx.message.content.split()
    user = ctx.message.author
    skill = ""
    if users[user.id]['skillPoints'] > 0:
        switcher = {
            "str" : "Strength",
            "int" : "Intelligence",
            "dex" : "Dexterity",
            "con" : "Constitution",
            "wis" : "Wisdom",
            "wil" : "WillPower",
            "per" : "Perception",
            "luc" : "Luck"
        }
        skill = switcher.get(messageEdit[1])

        users[user.id]['Skills'][skill] += 1
        users[user.id]['skillPoints'] -= 1
        
    else:
        await client.say("Not enough skill points, current skill points: "+str(users[user.id]['skillPoints']))
    
    with open('Levels.json','w') as f:
        json.dump(users,f)
    await printskills(ctx)
   
@client.command(pass_context=True)
async def skills(ctx):
    """ Prints your skills """
    await printskills(ctx)
    
async def printskills(ctx):
    user = ctx.message.author
    with open('Levels.json','r') as f:
        users = json.load(f)

    lvl = users[user.id]['level'] 
    sp = users[user.id]['skillPoints'] 
    strength = users[user.id]['Skills']['Strength'] 
    intell =users[user.id]['Skills']['Intelligence'] 
    dex = users[user.id]['Skills']['Dexterity'] 
    con = users[user.id]['Skills']['Constitution'] 
    wis = users[user.id]['Skills']['Wisdom'] 
    will = users[user.id]['Skills']['WillPower'] 
    per = users[user.id]['Skills']['Perception'] 
    luck = users[user.id]['Skills']['Luck'] 

    msg = ("Level: "+ str(lvl)+"\n" + "Skillpoints: "+ str(sp)+"\n"+"Strength: "+ str(strength)+"\n"+
    "Intelligence: "+ str(intell)+"\n"+"Dexterity : "+ str(dex)+"\n"+"Constitution: "+ str(con)+"\n" +
    "Wisdom: "+ str(wis) +"\n" +"Willpower: "+ str(will)+"\n" +"Perception: "+ str(per)+"\n" +"Luck: "+ str(luck)+"\n")
    await client.say(msg)

@client.command(pass_context=True)
async def working(ctx):
    """ This will check if the bot is working """
    await client.say("I'm working")

@client.command(pass_context=True)
async def hello(ctx):
    """ Messages back hello """
    msg = "Hello {0.author.mention}".format(ctx.message)
    await client.say(msg)
    
@client.command(pass_context=True)
async def poke(ctx):
    """ <Text> Repeats the string typed """
    messageEdit = ctx.message.content[5:]
    if messageEdit == None or messageEdit == "":
        await client.say("No message to poke with")
    msg = str(messageEdit)
    await client.say(msg)

@client.command(pass_context=True)
async def roll(ctx):
    """ Rolls d6 """
    messageEdit = ctx.message.content.split()
    del messageEdit[0]
    number = 0
    if len(messageEdit) == 0:
        number = 1
    else:
        number = int(messageEdit[0])

    if number > 20:
        await client.say("Max number of rolls is 20")
        return 

    rolled = ""
    for _ in range(0,number):
        rolled += (str(random.randint(1,6)) + " ")

    await client.say(rolled)

@client.command(pass_context=True)
async def member(ctx):
    """ Prints a random member from the discord """
    x = ctx.message.server.members
    y = random.randint(1,len(x))
    z = 0
    for member in x:
        if z == y :
            msg = member.name
            break
        elif z != y:
            z = z + 1
    await client.send_message(ctx.message.channel,msg)

@client.command(pass_context=True)
async def spam(ctx):
    """ <Text> Spams a message 5 times """
    messageEdit = ctx.message.content[5:]
    if messageEdit == None or messageEdit == "":
        await client.send_message(ctx.message.channel,"missing message")
    else:
        for _ in range(0,5):
            msg = messageEdit
            await client.send_message(ctx.message.channel,msg)

@client.command(pass_context=True)
async def spamx(ctx):
    """ <Number><Text> Spams a message <int> times """
    messageEdit = ctx.message.content[7:]
    messageArr = messageEdit.split()
    number = 0
    if len(messageArr) > 0:
        if messageArr[0].isdigit():
            number = int(messageArr[0]) 
        else:
            await client.send_message(ctx.message.channel,"Number then text")
            return
    else:
        await client.send_message(ctx.message.channel,"Number then text")
        return

    if messageEdit == None or messageEdit == "":
        await client.send_message(ctx.message.channel,"missing message")
        return

    if number >= 11:
        await client.send_message(ctx.message.channel,"max is 10")
        return

    for _ in range(0,number):
        msg = messageEdit[2:]
        await client.send_message(ctx.message.channel,msg)

@client.command(pass_context=True)
async def clear(ctx):
    """ Clears past 100 messages from bot and messages with * """
    logs = client.logs_from(ctx.message.channel,limit=99,before=None)
    msg  = []
    client.send_message(ctx.message.channel,"Collecting messages...")
    async for x in logs:
        if (x.content.startswith("*") or x.author == client.user) and not x.content.startswith("**"):
            msg.append(x)
    await client.delete_messages(msg)


@client.command(pass_context=True)
async def civ(ctx):
    """ <Number> prints three civs <Number> times """
    messageEdit = ctx.message.content.split()
    del messageEdit[0]
    chosenNum = []
    number = 0
    currentNumber = 0
    msg = ""
    if len(messageEdit) > 0:
        if messageEdit[0].isdigit():
            number = int(messageEdit[0]) 
        else:
            await client.send_message(ctx.message.channel,"I need a number")
            return
    else:
        await client.send_message(ctx.message.channel,"I need a number")
        return
    if number <= 14:
        while currentNumber < number:
            currentNumber = currentNumber + 1
            civ = civNum(chosenNum)
            chosenNum.append(civ)
            civ1 = civNum(chosenNum)
            chosenNum.append(civ1)
            civ2 = civNum(chosenNum)
            chosenNum.append(civ2)
            msg += str(currentNumber) + ": " + data["civilizations"][civ]["nationName"] + ", "+ data["civilizations"][civ1]["nationName"] + ", "+ data["civilizations"][civ2]["nationName"]+ "\n"
            
    else:
        await client.send_message(ctx.message.channel,"Not enough Civs, Max is 14")

    await client.send_message(ctx.message.channel,msg)

@client.command(pass_context=True)
async def magic8():
    """ Answers a yes/no question """
    response = ["Yes","Maybe","Ofc","Yes, you massive cuck","I don't know","Ask again","Try again","No, but you're boring","No","Only on a tuesday","Nope","I don't think so"]
    randomRe = random.randint(0,len(response)-1)
    await client.say(response[randomRe])

@client.command(pass_context=True)
async def quote():
    """ Prints a random discord quote """
    with open('Magic.json','r') as f:
        words = json.load(f)
    number = random.randint(0,len(words["word"])-1)
    await client.say(words["word"][number])

@client.command(pass_context=True)
async def addquote(ctx):
    """ Add a new message to the quote list """
    messageEdit = ctx.message.content[9:]
    with open('Magic.json','r') as f:
        words = json.load(f)

    words["word"].append(messageEdit)

    with open('Magic.json','w') as f:
        json.dump(words,f)
    
    await client.say("Added " + str(messageEdit) + " to the quote list")

@client.command(pass_context=True)
async def coinflip(ctx):
    """ <Number?> Flips a coin and prints results"""
    messageEdit = ctx.message.content.split()
    del messageEdit[0]
    number = 0
    if len(messageEdit) == 0:
        number = 1
    else:
        number = int(messageEdit[0])

    msg = ""
    if isinstance(number,int):
        if number <= 10:
            msg = ""
            for _ in range(0,number):
                randomed = random.randint(0,1)
                if randomed == 0:
                    msg += "Heads"+"\n"
                else:
                    msg += "Tails"+"\n"
                
        else:
            await client.say("I can't flip that many coins")
        await client.say(msg)
    
async def isOwner(ctx):
    if ctx.message.author.id == 65989446335463424:
        return True
    else:
        return False

@client.command(pass_context=True)
async def clout(ctx):
    """ Display plays amount of clout """
    with open('Levels.json','r') as f:
        users = json.load(f)
    cloutp = users[ctx.message.author.id]['CloutPoints']
    await client.say("You have {} clout coins".format(str(cloutp)))

@client.command(pass_context=True)
async def addclout(ctx):
    """ Give someone your clout
        *addclout @person
    """
    await client.say("Processing coin transfer")
    with open('Levels.json','r') as f:
        users = json.load(f)
    msg = ctx.message.content.split()
    unformattedReciever =  msg[1]
    reciever = unformattedReciever[2:-1]
    user = ctx.message.author

    if reciever[0] == "!":
        reciever = reciever[1:]

    if not reciever in users:
        await client.say("Invalid user, user doesn't exist in database {}".format(unformattedReciever))
    else:
        if users[user.id]['CloutPoints'] > 0:
            users[user.id]['CloutPoints'] -= 1
            users[user.id]['CloutStats']['Given'] += 1
            users[reciever]['CloutPoints'] += 1
            users[reciever]['CloutStats']['Gained'] += 1
            await client.say("{} you have received one new clout coin".format(unformattedReciever))
        else:
            await client.say("{} you don't have enough clout".format(user.mention))
    with open('Levels.json','w') as f:
        json.dump(users,f)

@client.command(pass_context=True)
async def vote(ctx):
    """ Vote for a certain topic after 30 seconds the verdict will be shown """
    msg = ctx.message.content[5:]
    vote = discord.Embed(title="****Polling****", description=msg,colour=0xDEADBF)
    vote.add_field(name="Time",value="30 seconds",inline=False)
    message = await client.send_message(ctx.message.channel,embed=vote)
    
    
    await client.add_reaction(message,"👍")
    await client.add_reaction(message,"👎")
    
    await asyncio.sleep(30)
    message_1 = await client.get_message(ctx.message.channel,message.id)

    good = 0
    bad = 0
    for reaction in message_1.reactions:
        if reaction.emoji == "👍":
            reactors = await client.get_reaction_users(reaction)
            for _ in reactors:
                good += 1
        if reaction.emoji == "👎":
            reactors = await client.get_reaction_users(reaction)
            for _ in reactors:
                bad += 1
    
    if good > bad:
        await client.say("Verdict: {} is correct".format(msg))
    elif bad > good:
        await client.say("Verdict: {} is incorrect".format(msg))
    else:
        await client.say("No verdict was met on: {}".format(msg))
    
@client.command(pass_context=True)
async def trivia(ctx):
    """ Random Trivia Question """
    with open('Trivia.json') as f:
        data = json.load(f)
    randomTrivia = data["Trivia"][random.randint(0,len(data["Trivia"])-1)]
    Question = randomTrivia["Question"]
    PossibleAnswers = randomTrivia["Answer"]
    answer = randomTrivia["CorrectAnswer"]
    random.shuffle(PossibleAnswers)
    msg = "{};   A: {}  B: {}  C: {}  D: {}".format(Question,PossibleAnswers[0],PossibleAnswers[1],PossibleAnswers[2],PossibleAnswers[3])
    message = await client.say(msg)

    await client.add_reaction(message,	u"\U0001F1E6")
    await client.add_reaction(message,	u"\U0001F1E7")
    await client.add_reaction(message,	u"\U0001F1E8")
    await client.add_reaction(message,	u"\U0001F1E9")

    await asyncio.sleep(20)
    location = ''
    if PossibleAnswers.index(answer) == 0:
        location = 'A'
    elif PossibleAnswers.index(answer) == 1:
        location = 'B'
    elif PossibleAnswers.index(answer) == 2:
        location = 'C'
    elif PossibleAnswers.index(answer) == 3:
        location = 'D'

    await client.say("The Correct Answer is {} {} ".format(str(location),answer))
    

@client.event
async def on_message(message): 
    message.content = message.content.lower()
    with open('Levels.json','r') as f:
        users = json.load(f)

    await update_data(users,message.author)
    await add_exp(users,message.author,5)
    await level_up(users, message.author,message.channel)

    with open('Levels.json','w') as f:
        json.dump(users,f)
    await client.process_commands(message)


client.run(token)